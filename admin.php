<?php
$realm = 'IHS Class of 2014 - Junior Prom 2012 Admin';

$users = array('ryan.tse' => 'rt-vrm-01','julianne.vinh' => 'youshouldbakeryansomecupcakes');

if (empty($_SERVER['PHP_AUTH_DIGEST'])) {
	header('HTTP/1.1 401 Unauthorized');
	header('WWW-Authenticate: Digest realm="'.$realm.
			'",qop="auth",nonce="'.uniqid().'",opaque="'.md5($realm).'"');

	die('Unauthorized login. An administrator has been notified of this event.');
}

if (!($data = http_digest_parse($_SERVER['PHP_AUTH_DIGEST'])) ||
	!isset($users[$data['username']])) {
	header('HTTP/1.1 401 Unauthorized');
	header('WWW-Authenticate: Digest realm="'.$realm.
			'",qop="auth",nonce="'.uniqid().'",opaque="'.md5($realm).'"');
	die('Unauthorized login. An administrator has been notified of this event.');
}

$A1 = md5($data['username'] . ':' . $realm . ':' . $users[$data['username']]);
$A2 = md5($_SERVER['REQUEST_METHOD'].':'.$data['uri']);
$valid_response = md5($A1.':'.$data['nonce'].':'.$data['nc'].':'.$data['cnonce'].':'.$data['qop'].':'.$A2);

if ($data['response'] != $valid_response) {
	header('HTTP/1.1 401 Unauthorized');
	header('WWW-Authenticate: Digest realm="'.$realm.
			'",qop="auth",nonce="'.uniqid().'",opaque="'.md5($realm).'"');
	die('Unauthorized login. An administrator has been notified of this event.');
}

function http_digest_parse($txt)
{
	$needed_parts = array('nonce'=>1, 'nc'=>1, 'cnonce'=>1, 'qop'=>1, 'username'=>1, 'uri'=>1, 'response'=>1);
	$data = array();
	$keys = implode('|', array_keys($needed_parts));

	preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $txt, $matches, PREG_SET_ORDER);

	foreach ($matches as $m) {
		$data[$m[1]] = $m[3] ? $m[3] : $m[4];
		unset($needed_parts[$m[1]]);
	}

	return $needed_parts ? false : $data;
}

function secs_to_h($secs)
{
		$units = array(
				"week"   => 7*24*3600,
				"day"    =>   24*3600,
				"hour"   =>      3600,
				"minute" =>        60,
				"second" =>         1,
		);
		
		if ( $secs == 0 ) return "0 seconds";

		$s = "";

		foreach ( $units as $name => $divisor ) {
				if ( $quot = intval($secs / $divisor) ) {
						$s .= "$quot $name";
						$s .= (abs($quot) > 1 ? "s" : "") . ", ";
						$secs -= $quot * $divisor;
				}
		}

		return substr($s, 0, -2);
}

function sort_votes($a, $b) {
	return $a["votecount"] - $b["votecount"];
}

if($_REQUEST["update"]==1) {

	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: POST");
	header("Access-Control-Allow-Headers: *");
	header("Content-Type: application/json");

	require("./mysql_catalyst.php");
	require("./mysql_alchemy.php");
	require("./mysql_blackmagic.php");
	require("./config.php");

	$data = array();


	try {

		$database = MySQLDatabase::getInstance();
		$database->connect("localhost","ihsc2014_voting","FMRxFbJDLqWjwdvZ", "ihs_c2014voting");

		$votes_array = array();
		$graph_array = array();

		$query = "SELECT * FROM ihs_options";
		foreach ($database->iterate($query) as $row) {
			$query = "SELECT COUNT(*) FROM ihs_votes WHERE ballot_castvote='".mysql_real_escape_string($row->option_id)."'";
			$num_votes = $database->fetchOne($query);
			$votes_array[] = array("title"=>$row->option_title, "votecount"=>$num_votes);
		}

	} catch (Exception $exception) {}

	$data["votes"] = $votes_array;

	usort($votes_array,"sort_votes");
	$data["leaderboard"] = array_reverse($votes_array);

	$facebook_data = @file_get_contents("http://api.facebook.com/restserver.php?method=links.getStats&urls=irvington2014.tumblr.com/vote&format=json");
	if($facebook_data == "") {
		$facebook_data = "null";
	} else {
		$facebook_data = json_decode($facebook_data, 1);
	}

	$data["facebook"] = $facebook_data;

	$load = sys_getloadavg();
	if($load[0] > 80) {
		$data["updatetime"] = "5000";
	} else {
		$data["updatetime"] = "1000";
	}

	if(time() < strtotime($start_time)) {
		$countdown_string = "Voting opens in ".secs_to_h(strtotime($start_time)-time()).".";
	} else if(time() < strtotime($end_time)) {
		$countdown_string = "Voting closes in ".secs_to_h(strtotime($end_time)-time()).".";
	} else if (time() > strtotime($end_time)) {
		$countdown_string = "Voting has already closed.";
	}

	$data["countdown"] = $countdown_string;

	die("update_views(".json_encode($data).");");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>IHS Class of 2014 - Junior Prom 2012 Admin</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

		<style type="text/css">
			html {
				overflow-y: scroll;
			}

			body {
				background: #b3c6d4;
				padding: 20px;
				font-family: "Century Gothic","Helvetica Neue",Helvetica,Arial,sans-serif;
				color: #4d4d4d;
			}

			#wrapper {
				width: 820px;
				margin: 0 auto;
			}

			#header {
				position: relative;
			}

			#header h1,
			.section h2,
			.section h3 {
				margin: 0;
				padding: 0;
				color: #162d50;
				font-size: 16px;
				font-weight: bold;
				margin-bottom: 5px;
			}

			#header h1{
				border-bottom: solid 1px #eee;
			}

			#stats h2 {
				margin-bottom: 10px;
			}

			#last-update { 
				position: absolute;
				top: 10px;
				right: 10px;
				font-size: 10px;
				font-weight: bold;
			}

			#server-name {
				font-weight: bold;
				font-size: 26px;
			}

			.section {
				margin-bottom: 20px;
				background: white;
				padding: 20px;
				-moz-border-radius: 5px;
				-webkit-border-radius: 5px;
			}
			
			.graphSection {
				margin-bottom: 20px;
				background: white;
				-moz-border-radius: 5px;
				-webkit-border-radius: 5px;
				border-radius: 5px;
			}

			.innerSection {
				float: left;
			}

			.clearfix { clear: both; }

			.valueContainer h4 {
				font-size: 12px;
				margin: 0;
				padding: 0;
			}

			.valueContainer {
				text-align: center;
				background: #f2f2f2;
				padding: 10px;
				-moz-border-radius: 3px;
				-webkit-border-radius: 3px;
				border-radius: 3px;
				float: left;
				margin-right: 10px;
				width: 100px;
			}

			.valueContainer div {
				font-size: 26px;
				font-weight: bold;
			}

			#connectionsTable {
				width: 100%;
				font-family: verdana, arial, helvetica, sans-serif;
				font-size: x-small;
				border-collapse: collapse;
				margin-top: 10px;
			}

			#connectionsTable tr th {
				background-color: #003366;
				color: #ffffff;
				font-weight: bold;
				text-align: left;
				padding: 0.5em;
				border-left: 1px dotted #cccccc;
				margin: 0;
			}

			#connectionsTable th:first-child {
				border-left: 0;
			}

			#connectionsTable tr th:hover {
				background-color: #2d5e92;
			}

			#connectionsTable td:first-child {
				border-left: 0;
			}

			#connectionsTable tr td {
				padding: 0.5em;
				border-left: 1px dotted #cccccc;
				margin: 0;
			}

			#connectionsTableEven td {
				background-color: #fff;
			}

			#connectionsTableOdd td {
				background-color: #eeeeee;
			}

		</style>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<script type="text/javascript">
		google.load("visualization", "1", {packages:["corechart"]});

		var graph_total = -1;
		var timeout_id = 0;

		jQuery.noConflict();
		jQuery(document).ready(function() {
			refresh();
		});

		function refresh() {
			jQuery.ajax({
				async: true,
				cached: false,
				dataType: "JSONP",
				url: self.location,
				data: {
					update: 1
				},
				error: function(xhr, ajaxOptions, thrownError) {
					clearTimeout(timeout_id);
					timeout_id=setTimeout(refresh, 1000);
				}
			});
		}

		function update_views(vdata) {
			var data = new google.visualization.DataTable();
			data.addColumn('string','Theme');
			data.addColumn('number','Votes');

			for(var i=0; i<vdata.votes.length; i++) {
				data.addRow([vdata.votes[i].title, +vdata.votes[i].votecount]);
			}

			var voteobject_data = "";
			var leaderboard_data = "";
			var total_votes = 0;

			for(var i=0; i<vdata.votes.length; i++) {
				voteobject_data = voteobject_data+"<div class='valueContainer'><h4>"+vdata.votes[i].title+"</h4><div>"+vdata.votes[i].votecount+"</div></div>";
				total_votes = total_votes+(+vdata.votes[i].votecount);
			}

			for(var i=0; i<vdata.leaderboard.length; i++) {
				leaderboard_data = leaderboard_data+"<div class='valueContainer'><h4>"+vdata.leaderboard[i].title+"</h4><div>"+vdata.leaderboard[i].votecount+"</div></div>";
			}

			jQuery("#votes-objects").html(voteobject_data);
			jQuery("#leader-objects").html(leaderboard_data);

			jQuery("#votecount").html("Total Votes: "+(+total_votes)+"<br />"+vdata.countdown);

			if(vdata.facebook.length>0) {
				jQuery("#fb-share-stats").html("<div class='valueContainer'><h4>Shares</h4><div>"+vdata.facebook[0].share_count+"</div></div><div class='valueContainer'><h4>Likes</h4><div>"+vdata.facebook[0].like_count+"</div></div><div class='valueContainer'><h4>Comments</h4><div>"+vdata.facebook[0].comment_count+"</div></div><div class='valueContainer'><h4>Total</h4><div>"+vdata.facebook[0].total_count+"</div></div>");
			}

			if(total_votes>0) {
				if(total_votes !== graph_total) {
					jQuery("#no_data").hide();
					jQuery("#chart_div").show();
					var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
					chart.draw(data);
					graph_total=total_votes;
				}
			} else {
				graph_total=-1;
				jQuery("#no_data").show();
				jQuery("#chart_div").hide();
			}

			var date = new Date();
			jQuery("#last-update").text("Last Updated: "+date.toLocaleString());
			timeout_id=setTimeout(refresh, (+vdata.updatetime));
		}
		</script>
	</head>
	<body>
		<div id="wrapper">
			<div id="header" class="section">
				<h1 id="docTitle">Statistics</h1>
				<div id="last-update"></div>
				<div id="votecount"></div>
				<div class="clearfix"></div>
			</div>
			<div id="facebook" class="section">
				<h2>Facebook Reach</h2>
				<div class="innerSection" id="fb-share-stats">
				</div>
				<div class="clearfix"></div>
			</div>
			<div id="leaders" class="section">
				<h2>Votes (Leaderboard)</h2>
				<div class="innerSection" id="leader-objects">
				</div>
				<div class="clearfix"></div>
			</div>
			<div id="votes" class="section" style="display:none">
				<h2>Votes</h2>
				<div class="innerSection" id="votes-objects">
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="section">
				<h2>Graph</h2>
				<div id="chart_div" class="graphSection">
				</div>
				<div id="no_data">
					<br />No data available.<br /><br />
				</div>
			</div>
		</div>
	</body>
</html>