<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: *");
header("Content-Type: application/json");

require("./mysql_catalyst.php");
require("./mysql_alchemy.php");
require("./mysql_blackmagic.php");
require("./config.php");

try {
	if(time() < strtotime($start_time)) {
		throw new Exception('{"status": "failure", "voteerror":"Sorry, the voting period has not started yet. Voting starts on Monday, December 3rd, 2012 at 8:00 AM PST. Please try again later."}');
	}
	if(time() > strtotime($end_time)) {
		throw new Exception('{"status": "failure", "voteerror":"Sorry, the voting period has already expired."}');
	}

	$last_name                       = preg_replace("/\s+/"," ",strtoupper(trim($_REQUEST["lastname"])));
	$id_number                       = intval(trim($_REQUEST["idnumber"]));
	$cast_value                      = intval($_REQUEST["themechoice"]);
	
	$submit_data_array               = array();
	$submit_data_array["time"]       = time();
	$submit_data_array["ip_address"] = $_SERVER["REMOTE_ADDR"];
	$submit_data_array["ip_port"]    = $_SERVER["REMOTE_PORT"];

	$submit_data = json_encode($submit_data_array);

	try {
		$database = MySQLDatabase::getInstance();
		$database->connect("localhost","ihsc2014_voting","FMRxFbJDLqWjwdvZ", "ihs_c2014voting");
	} catch (MySQLException $exception) {
		throw new Exception('{"status": "failure", "voteerror":"An unknown error occurred. Please try again later."}');
	}

	$query = "SELECT COUNT(*) FROM ihs_students WHERE student_id='".mysql_real_escape_string($id_number)."' AND last_name='".mysql_real_escape_string($last_name)."'";
	try {
		if($database->fetchOne($query) == 0) {
			throw new Exception('{"status": "failure", "voteerror":"We were unable to find you on our student list. Please check your last name and ID number."}');
		}
	} catch (MySQLException $exception) {
		throw new Exception('{"status": "failure", "voteerror":"An unknown error occurred. Please try again later."}');
	}

	$query = "SELECT status FROM ihs_students WHERE student_id='".mysql_real_escape_string($id_number)."' AND last_name='".mysql_real_escape_string($last_name)."' LIMIT 1";
	try {
		$availibility = $database->fetchOne($query);
		if($availibility == 1) {
			event_log("CAST_DUPLICATE",array("id_number"=>$id_number, "last_name"=>$last_name, "cast_value"=>$cast_value));
			throw new Exception('{"status": "failure", "voteerror":"Sorry, you have already voted. One vote per student."}');
		}
	} catch (MySQLException $exception) {
		throw new Exception('{"status": "failure", "voteerror":"An unknown error occurred. Please try again later."}');
	}

	if($_REQUEST["themechoice"]=="") {
		throw new Exception('{"status": "failure", "voteerror":"Please select a theme and try again."}');
	}

	$query = "SELECT COUNT(*) FROM ihs_options WHERE option_id='".mysql_real_escape_string($cast_value)."'";
	try {
		if($database->fetchOne($query) == 0) {
			event_log("CAST_NONEXISTENT",array("id_number"=>$id_number, "last_name"=>$last_name, "cast_value"=>$cast_value));
			echo "initialize_themes();\n";
			throw new Exception('{"status": "failure", "voteerror":"The theme you selected does not exist. Please try again."}');
		}
	} catch (MySQLException $exception) {
		throw new Exception('{"status": "failure", "voteerror":"An unknown error occurred. Please try again later."}');
	}

	$query = "INSERT INTO ihs_votes (ballot_castvote, ballot_data) VALUES ('".mysql_real_escape_string($cast_value)."','".mysql_real_escape_string($submit_data)."')";
	try {
		$ballot_id = $database->insert($query);
		event_log("CAST_SUCCESS",array("id_number"=>$id_number, "last_name"=>$last_name, "cast_value"=>$cast_value, "ballot_id"=>$ballot_id));
	} catch (MySQLException $exception) {
		throw new Exception('{"status":"failure", "voteerror":"An unknown error occurred. Please try again later."}');
	}

	$query = "UPDATE ihs_students SET status='1' WHERE student_id='".mysql_real_escape_string($id_number)."' AND last_name='".mysql_real_escape_string($last_name)."' LIMIT 1";
	try {
		$ballot_id = $database->query($query);
	} catch (MySQLException $exception) {
		event_log("CAST_NOUPDATE",array("id_number"=>$id_number, "last_name"=>$last_name, "cast_value"=>$cast_value, "ballot_id"=>$ballot_id));
		throw new Exception('{"status":"failure", "voteerror":"An unknown error occurred. Please try again later."}');
	}
	$return_status = '{"status":"success"}';

} catch (Exception $exception) {
	$return_status = $exception->getMessage();
}

function event_log($event_name, $event_data = array()) {
	$event_database           = MySQLDatabase::getInstance();
	$event_data["time"]       = time();
	$event_data["ip_address"] = $_SERVER["REMOTE_ADDR"];
	$event_data["ip_port"]    = $_SERVER["REMOTE_PORT"];

	try {
		$query = "INSERT INTO ihs_eventlog (event_type, event_details) VALUES ('".mysql_real_escape_string($event_name)."','".mysql_real_escape_string(json_encode($event_data))."')";
		$event_database->insert($query);
	} catch (Exception $exception) {}
}

?>
cast_process(<?php echo $return_status; ?>);