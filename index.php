<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta property="og:title" content="Junior Prom 2012 - Theme Voting" />
		<meta property="og:description" content="Vote for your favorite theme for this year's junior prom. Every vote matters so join in and cast yours now!" />
		<meta property="og:image" content="https://platform.ryantse.me/c2014/prom-2012/fb-vote.png" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="http://irvington2014.tumblr.com/vote" />

		<title>IHS Class of 2014 - Junior Prom 2012</title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<link rel="stylesheet" href="https://platform.ryantse.me/c2014/prom-2012/platform.css" media="all" />
		<link rel="stylesheet" href="https://platform.ryantse.me/c2014/prom-2012/custom.css" media="all" />
		<link href='https://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

		<!--[if lt IE 9]>
		<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<script src="https://platform.ryantse.me/c2014/prom-2012/platform.js"></script>
	</head>
	<body>
		<div class="wrapper">
			<header id="header" class="group">
				<h1 id="header-home">IHS Class of 2014 - Junior Prom 2012</h1>
				<nav>
					<ul>
						<li><a href="#" id="view-vote">Vote</a></li>
						<li><a href="#" id="view-privacypolicy">Privacy Policy</a></li>
					</ul>
			</header>
		</div>

		<div id="outer">
			<div id="voting" class="wrapper">
				<div class="row">
					<div class="third">
						<div id="vote">
							<h3>Cast Your Vote!</h3>
							<div class="forms">
								<ul>
									<li id="voteerror" class="red">
									</li>
									<li>
										<label for="lastname">Last Name</label>
										<input type="text" name="lastname" id="lastname" class="width-100" style="height:27px"/>
									</li>
									<li>
										<label for="idnumber">ID Number</label>
										<input type="text" name="idnumber" id="idnumber" class="width-100" style="height:27px"/>
									</li>
									<li>
										<label for="themechoice">Theme Choice (<a id="descriptionslink" href="#themechoices">Descriptions</a>)</label>
										<select id="themechoice" name="themechoice" class="width-100" style="height:27px"/>
										</select>
									<li>
										<br />
										<input type="button" id="votebutton" value="Vote" class="btn" />
									</li>
								</ul>
							</div>
							<small>Note: Only juniors are eligible to vote; one vote per student. All votes are cast by secret ballot, your last name and ID number are used to track who has voted.</small>
						</div>
						<div id="thankyou">
							<h3>Thank You.</h3>
							We really appreciate your participation! And, remember to encourage your fellow classmates to vote as well.<br /><br />
							<img src="https://platform.ryantse.me/c2014/prom-2012/fb-share.png" alt="Share it on Facebook." id="fb-share"/>
						</div>
					</div>
					<div class="twothird" id="themechoices">
					
						<h2>Theme Choices</h2>
						
						<article id="blocks">
						</article>
					
					</div>

				</div>
			</div>
		</div>
		<div id="privacypolicy">
			<div class="wrapper">
				<h2>Privacy Policy</h2>
				<strong>What information do we collect?</strong> <br /><br />We collect information from you when you respond to a survey or fill out a form.  <br /><br />When ordering or registering on our site, as appropriate, you may be asked to enter your: name or ID Number. You may, however, visit our site anonymously.<br /><br /><strong>What do we use your information for?</strong> <br /><br />Any of the information we collect from you may be used in one of the following ways: <br /><br /><ul><li>To personalize your experience<br />(your information helps us to better respond to your individual needs);</li><li>To improve our website<br />(we continually strive to improve our website offerings based on the information and feedback we receive from you);</li><li>To improve customer service<br />(your information helps us to more effectively respond to your customer service requests and support needs);</li><li>To administer a contest, promotion, survey or other site feature.</li></ul><br /><strong>How do we protect your information?</strong> <br /><br />We implement a variety of security measures to maintain the safety of your personal information when you enter, submit, or access your personal information. <br /> <br />We offer the use of a secure server. All supplied sensitive/credit information is transmitted via Secure Socket Layer (SSL) technology and then encrypted into our Database to be only accessed by those authorized with special access rights to our systems, and are required to keep the information confidential. <br /><br />After a transaction, your private information (credit cards, social security numbers, financials, etc.) will not be stored on our servers.<br /><br /><strong>Do we use cookies?</strong> <br /><br />Yes (Cookies are small files that a site or its service provider transfers to your computers hard drive through your Web browser (if you allow) that enables the sites or service providers systems to recognize your browser and capture and remember certain information)<br /><br /> We use cookies to help us remember and process the items in your shopping cart, understand and save your preferences for future visits, keep track of advertisements and compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future. We may contract with third-party service providers to assist us in better understanding our site visitors. These service providers are not permitted to use the information collected on our behalf except to help us conduct and improve our business.<br /><br /><strong>Do we disclose any information to outside parties?</strong> <br /><br />We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.<br /><br /><strong>Third Party Links</strong> <br /><br /> Occasionally, at our discretion, we may include or offer third party products or services on our website. These third party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.<br /><br /><strong>California Online Privacy Protection Act Compliance</strong><br /><br />Because we value your privacy we have taken the necessary precautions to be in compliance with the California Online Privacy Protection Act. We therefore will not distribute your personal information to outside parties without your consent.<br /><br /><strong>Childrens Online Privacy Protection Act Compliance</strong> <br /><br />We are in compliance with the requirements of COPPA (Childrens Online Privacy Protection Act); we do not collect any information from anyone under 13 years of age. Our website, products and services are all directed to people who are at least 13 years old or older.<br /><br /><strong>Your Consent</strong> <br /><br />By using our site, you consent to our web site privacy policy.<br /><br /><strong>Changes to our Privacy Policy</strong> <br /><br />If we decide to change our privacy policy, we will post those changes on this page, and/or update the Privacy Policy modification date below. <br /><br />This policy was last modified on October 21st, 2012.<br /><br /><strong>Contacting Us</strong> <br /><br />If there are any questions regarding this privacy policy you may contact us using the information below. <br /><br />41800 Blacow Road<br />Fremont, California 94538<br />United States<br /><br />
			</div>
		</div>
		<footer id="footer">

			<div class="wrapper">
				<section>
					&copy; IHS Class of 2014. Website written by Ryan Tse.
					<script type="text/javascript">
					var pkBaseURL = (("https:" == document.location.protocol) ? "https://analytics.ryantse.me/" : "https://analytics.ryantse.me/");
					document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
					</script><script type="text/javascript">
					try {
					var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 6);
					piwikTracker.trackPageView();
					piwikTracker.enableLinkTracking();
					} catch( err ) {}
					</script>
					<noscript><img src="https://analytics.ryantse.me/piwik.php?idsite=6" style="border:0" alt="" /></noscript>
				</section>
			</div>
		</footer>
	</body>
</html>
