jQuery.noConflict();

jQuery(document).ready(function() {
	if(top.location != self.location) {
		top.location = self.location.href;
	}
	initialize_links();
	initialize_themes();
});

function initialize_links() {
	jQuery("#view-vote, #header-home").click(function(e) {
		jQuery("#voting").show();
		jQuery("#privacypolicy").hide();
		e.preventDefault();
	});

	jQuery("#view-privacypolicy").click(function(e) {
		jQuery("#voting").hide();
		jQuery("#privacypolicy").show();
		e.preventDefault();
	});

	jQuery("#votebutton").click(function(e) {
		submit_vote();
		e.preventDefault();
	});

	jQuery("#fb-share").click(function(e) {
		share_vote();
		e.preventDefault();
	});

	jQuery("#descriptionslink").click(function(e) {
		var description_location = jQuery("#themechoices").offset();
		window.scrollTo(description_location.left, description_location.top);
		e.preventDefault();
	});

	jQuery("a").attr("tabindex","-1");
	jQuery("#lastname").attr("tabindex","1");
	jQuery("#idnumber").attr("tabindex","2");
	jQuery("#themechoice").attr("tabindex","3");
	jQuery("#votebutton").attr("tabindex","4");
}

function initialize_themes() {
	jQuery.ajax({
		async: true,
		cached: false,
		dataType: "JSONP",
		url: "https://platform.ryantse.me/c2014/prom-2012/platform.php",
	});
}

function update_themes(data) {
	if(data.length>0) {
		jQuery("#blocks").html("");
		jQuery("#themechoice").html("");
	}
	for(var i=0; i<data.length; i++) {
		jQuery("#blocks").append("<dl><dt>"+data[i].title+"</dt><dd>"+data[i].description+"</dd></dl>");
		jQuery("#themechoice").append("<option value='"+data[i].id+"'>"+data[i].title+"</option>");
	}
	jQuery("#themechoice").prop("selectedIndex",-1);
}

function submit_vote() {
	jQuery("#votebutton").attr("disabled","disabled");
	jQuery("#voteerror").hide();
	jQuery.ajax({
		async: true,
		cached: false,
		dataType: "JSONP",
		url: "https://platform.ryantse.me/c2014/prom-2012/cast.php",
		data: {
			lastname: jQuery("#lastname").val(),
			idnumber: jQuery("#idnumber").val(),
			themechoice: jQuery("#themechoice").val()
		}
	});
}

function share_vote() {
	window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(top.location), 'facebook_share', 'height=320, width=640, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no');
}

function cast_process(data) {
	switch(data.status) {
		case "success":
			jQuery("#vote").hide();
			jQuery("#thankyou").show();
			var thankyou_location = jQuery("#thankyou").offset();
			window.scrollTo(thankyou_location.left, thankyou_location.top);
			break;

		case "failure":
			jQuery("#voteerror").html(data.voteerror);
			jQuery("#voteerror").show();
			var voteerror_location = jQuery("#voteerror").offset();
			window.scrollTo(voteerror_location.left, voteerror_location.top);
			jQuery("#votebutton").removeAttr("disabled");
			break;

		default:
			jQuery("#voteerror").html("A general error occurred. Please try again later.");
			jQuery("#voteerror").show();
			var voteerror_location = jQuery("#voteerror").offset();
			window.scrollTo(voteerror_location.left, voteerror_location.top);
			jQuery("#votebutton").removeAttr("disabled");
			break;
	}
}