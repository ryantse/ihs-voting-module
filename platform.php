<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: *");
header("Content-Type: application/json");

require("./mysql_catalyst.php");
require("./mysql_alchemy.php");
require("./mysql_blackmagic.php");

try {
	$database = MySQLDatabase::getInstance();
	$database->connect("localhost","ihsc2014_voting","FMRxFbJDLqWjwdvZ", "ihs_c2014voting");

	$options_array = array();

	$query = "SELECT * FROM ihs_options";
	foreach ($database->iterate($query) as $row) {
		$options_array[] = array("id"=>$row->option_id, "title"=>$row->option_title, "description"=>$row->option_desc);
	}

	$result_array = json_encode($options_array);
	echo "update_themes(".$result_array.");";
} catch (Exception $exception) { }
?>